# Quelle situation professionnelle après un parcours en insertion par l'activité économique ? (Dares Analyses 2024-9)


-   Auteur : Julien Blasco

Ce dossier contient les programmes ayant servi à produire les illustrations ainsi que les chiffres cités dans le texte du Dares Analyses n°2024-9 "Quelle situation professionnelle après un parcours en insertion par l'activité économique ?".

Lien vers l'étude :

[Présentation de la Dares](https://dares.travail-emploi.gouv.fr/qui-sommes-nous) : La Dares est la Direction de l’animation de la recherche, des études et des statistiques du ministère du Travail, de la Santé et des Solidarités. Elle contribue à la conception, au suivi et à l’évaluation des politiques publiques, et plus largement à éclairer le débat économique et social. 

[Source de données](https://dares.travail-emploi.gouv.fr/enquete-source/les-sortants-de-linsertion-par-lactivite-economique) : enquête auprès des sortants de l’Insertion par l’activité économique (IAE)  (Dares). Calculs Dares.

------------------------------------------------------------------------

## Structure du code

Le code de ce Dares Analyses est constitué de 1 programme principal et 9 sous-programmes :

-   00_Main.R
-   01_Définition_paramètres.R : paramètrage
-   02_Chargement-bases.R : copie des données dans le répertoire local
-   11_Concaténation_bases.R : fusion des données
-   12_Traitement_non_réponse.R : correction de la non réponse
-   13_Calcul_variables_supplementaires.R : enrichissements des données
-   14_Recodage_variables_categ.R : recodage en variables catégorielles
-   15_Sous_tableaux.R : Création des sous-tableaux
-   21_Carte.R : cartes
-   35_Figures_V4.R : illustrations et graphiques

------------------------------------------------------------------------

## Paramètres à surcharger et fichiers externes

Fichier secrets.yaml.template est à renommer en secrets.yaml et à renseigner :

-   chemin_bases_redress : chemin vers le répertoire des données
-   chemin_fichier_archive_ASP : chemin vers l'archive ASP
-   chemin_fichier_categories : chemin vers les fichiers de catégories
-   chemin_fichier_fond_cartes : chemin vers les fonds de cartes

une version des fichiers du ROME est disponible ici : <https://www.data.gouv.fr/fr/datasets/r/8cf674b6-ef21-446a-8190-178e2defd6fc>

------------------------------------------------------------------------

## Installation des librairies R

Les librairies peuvent être installés pour ce projet avec renv avec la commande :

```         
renv::restore()
```

Les programmes ont été exécutés pour la dernière fois avec le logiciel R, le 16/01/2024.
