btnr <- read_fst(paste0(interm, "/12_base_totale_nr.fst"))

# Missions ----------------------------------------------------------------

chemin_fichiers_asp <- "0_Donnees"

fichiers_ASP <- c("fluxIAE_Missions_09012023.csv", 
                  "fluxIAE_MissionsEtatMensuelIndiv_09012023.csv", 
                  "fluxIAE_ContratMission_09012023.csv")

if (!all(fichiers_ASP %in% list.files(chemin_fichiers_asp))) {
  archive_ASP <- yaml::read_yaml("secrets.yaml")$chemin_fichier_archive_ASP
  archive_extract(archive_ASP, dir=chemin_fichiers_asp, files=fichiers_ASP)
}

missions <- data.table::fread(file.path(chemin_fichiers_asp, fichiers_ASP[1]))
missions_mens <- data.table::fread(file.path(chemin_fichiers_asp, fichiers_ASP[2]))
contrats <- data.table::fread(file.path(chemin_fichiers_asp, fichiers_ASP[3]))
contrats_selectionnees <- contrats %>% 
  select(contrat_id_pph,
         contrat_id_ctr, 
         contrat_niveau_de_formation_code, 
         contrat_salarie_aide_sociale,
         contrat_salarie_ass,
         contrat_salarie_rsa,
         contrat_salarie_aah,
         contrat_nb_heures_accomp,
         contrat_duree_hebdo_travail,
         contrat_mesure_disp_code,
         contrat_orienteur_code,
         contrat_date_embauche,
         contrat_date_sortie_definitive,
         contrat_date_fin_contrat
         )

duree_parcours <- contrats_selectionnees %>% 
  inner_join(select(btnr, id_salarie, struct, date_fin_reelle), 
            by=c("contrat_id_pph"="id_salarie")) %>% 
  mutate(date_fin = dmy(ifelse(contrat_date_sortie_definitive=="",
                               contrat_date_fin_contrat,
                               contrat_date_sortie_definitive))) %>% 
  mutate(duree_contrat_jours = pmax(0, pmin(date_fin,date_fin_reelle) -
           dmy(contrat_date_embauche))) %>% 
  summarise(
    id_salarie = ffirst(contrat_id_pph, g=contrat_id_pph),
    duree_contrats = fsum(duree_contrat_jours, g=contrat_id_pph)
  )

secteurs_principaux <- missions %>% 
  mutate(
    rome1 = substr(mission_code_rome, 1, 1),
    duree = interval(dmy(mission_date_debut), dmy(mission_date_fin))/days(1),
    duree_totale = collapse::fsum(duree, g=list(mission_id_ctr, rome1), TRA=1),
    duree_totale_max = collapse::fmax(duree_totale, g=mission_id_ctr, TRA=1)
  ) %>% 
  filter(duree_totale == duree_totale_max) %>% 
  group_by(mission_id_ctr) %>% 
  summarise(across(c(rome1, mission_code_rome), first))

missions_duree_hebdo <- missions %>% 
  left_join(missions_mens, by=c("mission_id_mis"="mei_mis_id")) %>% 
  mutate(date_debut_mission = dmy(mission_date_debut),
         date_fin_mission = dmy(mission_date_fin)) %>% 
  group_by(mission_id_ctr) %>% 
  summarise(
    date_debut_premiere_mission = fmin(date_debut_mission),
    date_fin_derniere_mission = fmax(date_fin_mission),
    nb_heures = fsum(mei_nombre_heures)
  ) %>% 
  mutate(
    date_debut_premiere_mission = as_date(date_debut_premiere_mission),
    date_fin_derniere_mission = as_date(date_fin_derniere_mission)
  )

cat_hebdo <- c("Quotité faible", "Quotité moyenne", "Quotité élevée")

btnrvar <- btnr %>% 
  mutate (
    rsa_pe = ifelse(rsa_dept=="O", 2, 0),
    ass_pe = ifelse(ass=="O", 2, 0),
    aah_pe = ifelse(aah=="O", 2, 0),
    deld_pe = case_when(Duree_pole_emploi=="04" ~ 2, 
                        Duree_pole_emploi=="03" ~ 1, 
                        Duree_pole_emploi=="00" ~ 0, 
                        Duree_pole_emploi=="01" ~ 0, 
                        Duree_pole_emploi=="02" ~ 0),
    niv_form_pe = case_when(niv_form_c==1 ~ 1, 
                            niv_form_c==2 ~ 1, 
                            niv_form_c==3 ~ 0, 
                            niv_form_c==4 ~ 0),
    age_pe = case_when (age_tr=="00-25 ans" ~ 1, 
                        age_tr=="55 ans et plus" ~ 1, 
                        age_tr=="26-30 ans" ~ 0, 
                        age_tr=="31-40 ans" ~ 0, 
                        age_tr=="41-50 ans" ~ 0, 
                        age_tr=="51-55 ans" ~ 0),
    handicape_pe = ifelse(handicape=="O", 1, 0),
    zrr_pe = ifelse(ident_ZRR=="tr", 1, 0),
    qpv_pe = ifelse(qpv_c==1, 1, 0),
    
    score_pe = rsa_pe + ass_pe + aah_pe + deld_pe + niv_form_pe + 
      age_pe + handicape_pe + zrr_pe + qpv_pe
  ) %>% 
  group_by(annee_sortie, siret) %>% 
  mutate(
    nombre_sal_siret=n(),
    score_pe_siret = mean(score_pe),
    score_pe_siret2=((score_pe_siret*nombre_sal_siret)-score_pe)/(nombre_sal_siret-1)
  ) %>% 
  group_by(annee_sortie, struct) %>% 
  mutate(score_pe_siret_cat = weighted_ntile(score_pe_siret2, poidscale, 4)) %>% 
  ungroup() %>%
  left_join(missions_duree_hebdo, by=c("id_contrat"="mission_id_ctr")) %>% 
  mutate(
    duree_hebdo2 = ifelse(
      struct=="AI" | struct=="ETTI", 
      nb_heures/(ceiling((date_fin_derniere_mission-date_debut_premiere_mission)/ddays(7))), 
      duree_hebdo ),
    duree_hebdo3 = ifelse(
      (struct=="AI" | struct=="ETTI") & (date_fin_derniere_mission-date_debut_premiere_mission)/ddays(7)==0, 
      nb_heures/(1/7), 
      duree_hebdo2),
    duree_hebdo_cat = case_when(
      struct == "ACI" ~ cut(duree_hebdo3, c(-Inf,25,26,Inf), labels=paste(cat_hebdo, c(
        "(25 heures ou moins)", "(26 heures)", "(plus de 26 heures)"))),
      struct == "EI" ~ cut(duree_hebdo3, c(-Inf,25,34,Inf), labels=paste(cat_hebdo, c(
        "(25 heures ou moins)", "(entre 25 et 34 heures)", "(plus de 34 heures)"
      ))),
      struct == "AI" ~ cut(duree_hebdo3, c(-Inf,6,14,Inf), labels=paste(cat_hebdo, c(
        "(6 heures ou moins)", "(entre 6 et 14 heures)", "(plus de 14 heures)"
      ))),
      struct == "ETTI" ~ cut(duree_hebdo3, c(-Inf,16,28,Inf), labels=paste(cat_hebdo,c(
        "(16 heures ou moins)", "(entre 16 et 28 heures)", "(plus de 28 heures)"
      )))
    ),
    duree_hebdo_cat_ident = case_when(
      struct == "ACI" ~ cut(duree_hebdo3, c(-Inf,25,26,Inf), labels=cat_hebdo),
      struct == "EI" ~ cut(duree_hebdo3, c(-Inf,25,34,Inf), labels=cat_hebdo),
      struct == "AI" ~ cut(duree_hebdo3, c(-Inf,6,14,Inf), labels=cat_hebdo),
      struct == "ETTI" ~ cut(duree_hebdo3, c(-Inf,16,28,Inf), labels=cat_hebdo)
    )
  ) %>% 
  left_join(secteurs_principaux, by=c("id_contrat"="mission_id_ctr")) %>% 
  left_join(contrats_selectionnees, by=c("id_contrat"="contrat_id_ctr")) %>% 
  left_join(duree_parcours, by=c("id_salarie"="id_salarie"))

# Ecriture de la table ----------------------------------------------------

write_fst(btnrvar, paste0(interm, "/13_base_totale_nr_var.fst"))