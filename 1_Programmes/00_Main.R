## Projet : Dares Analyses Insertion professionnelle des sortants de l'IAE 
##
## Nom du programme : 00_Main.R
##
## Auteur : Julien Blasco
## Date de création : 9 août 2022 (repris le 27 septembre pour DA 2020/2021)
##
## Description : 
## Ce programme contient toutes les étapes nécessaires à la construction
## des fichiers relatifs au DA sur l'insertion
##

# 01 - Définition des fonctions et paramètres utiles ----------------------
source("./1_Programmes/01_Définition_paramètres.R", encoding = "UTF-8")

# 02 - Récupération des bases sur W si pas en local -----------------------
source("./1_Programmes/02_Chargement-bases.R", encoding = "UTF-8")


# 11 - Concaténation des bases par année et structure --------------------------
lance_programme("./1_Programmes/11_Concaténation_bases.R", dependances = c(
  paste0(dossier_donnees, "/", c("aci", "ei", "ai", "etti"), "_app_cale_30_140.sas7bdat"),
  paste0(dossier_donnees, "/", c("aci", "ei", "ai", "etti"), "_app_cale_150_260.sas7bdat"),
  paste0(dossier_donnees, "/", c("aci", "ei", "ai", "etti"), "_app_cale_270_380.sas7bdat")
))

# 12 - Prise en compte de la NR partielle sur les variables formation/accomp --------
lance_programme("./1_Programmes/12_Traitement_non_réponse.R", 
                dependances = paste0(interm, "/11_base_totale.fst"))

# 13 - Calcul de variables supplémentaires  --------
lance_programme("./1_Programmes/13_Calcul_variables_supplementaires.R", 
                dependances = paste0(interm, "/12_base_totale_nr.fst"))

# 14 - Recodage de variables catégorielles  --------
lance_programme("./1_Programmes/14_Recodage_variables_categ.R", 
                dependances = paste0(interm, "/13_base_totale_nr_var.fst"))

# 15 - Construction de sous-tableaux  --------
lance_programme("./1_Programmes/15_Sous_tableaux.R", 
                dependances = paste0(interm, "/14_base_totale_nr_var_recod.fst"))

# 21 - Création de la carte ----------------------------------------
lance_programme("./1_Programmes/21_Carte.R", 
                dependances = paste0(interm, "/14_base_totale_nr_var_recod.fst"))

# 32 - Construction des figures V1 ----------------------------------------
lance_programme("./1_Programmes/35_Figures_V4.R", 
                dependances = c(
                  paste0(interm, "/14_base_totale_nr_var_recod.fst"),
                  paste0(interm, "/21_plot_total.rds"),
                  paste0(interm, "/15_sortants2021.fst"),
                  paste0(interm, "/15_devenir_pro.fst")
                  ))